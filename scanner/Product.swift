//
//  Product.swift
//  scanner
//
//  Created by Anh Duong on 27.8.2019.
//  Copyright © 2019 Anh Duong. All rights reserved.
//


struct Product : Codable {
    var packaging: String?
    let productName: String
    
    private enum CodingKeys: String, CodingKey {
       case packaging
       case productName = "product_name"
    }
}
