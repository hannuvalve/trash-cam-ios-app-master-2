//
//  HomeViewController.swift
//  scanner
//
//  Created by Anh Duong on 23.8.2019.
//  Copyright © 2019 Anh Duong. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
    

    var productCode: String = ""
    
    //MARK: Properties
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var productNameLabel: UILabel!

    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var productPackage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if (!productCode.isEmpty) {
            let BASE_URL = "https://world.openfoodfacts.org/api/"
            let PRODUCT_ENDPOINT = "v0/product/"
            let getProductFromBarcode = BASE_URL + PRODUCT_ENDPOINT + productCode
            Alamofire.AF.request(getProductFromBarcode).response { response in
                guard let data = response.data else { return }
                do {
                    let decoder = JSONDecoder()
                    let productResponse = try decoder.decode(ProductResponse.self, from: data)
                    self.productName.text = productResponse.product.productName
                    self.productPackage.text = productResponse.product.packaging!
                    
                    if (productResponse.status == 1) {
                        self.productName.text = productResponse.product.productName
                        if ((productResponse.product.packaging ?? "").isEmpty) {
                            self.productPackage.text = "No packaging details provided."
                        } else {
                            self.productPackage.text = productResponse.product.packaging
                        }
                    } else {
                        self.productName.text = "No product found."
                        self.productPackage.text = "No packaging details provided."
                    }
                } catch let error {
                    print(error)
                }
            }
        } else {
            self.productName.text = "Scan barcode to get product's name"
            self.productPackage.text = "Scan barcode to get product's package"
        }
    }
    
    //MARK: Actions
    @IBAction func openScanner(_ sender: UIButton) {
        productCode = ""
        let scannerScreen = ScannerViewController()
        self.navigationController?.pushViewController(scannerScreen, animated: true)
    }
    
}
